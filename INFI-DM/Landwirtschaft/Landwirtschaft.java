import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Landwirtschaft {
	
	private static Connection c;
	
	public static void main(String[] args) {
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection("jdbc:sqlite:landwirtschaft.db");
			
			System.out.println("Opened Database successfully");
			
			loeschen();
			
			createTableLandwirtschaft();
			createTableStall();
			createTableBesitzer();
			createTableTiere();
			createTableStallTiere();
			createTableBesitzerTiere();
			
			insertLandwirtschaft("Peter","Pan",1234567890,"Peter-Pan-Straße",123,"PanBauer");
			insertStall("PanStall1",25, 19, 1484672876, 5.4);
			insertBesitzer("Peter", "Pan", 1234567890, "Peter-Pan-Straße", 123);
			insertTiere("Samuel", 1074359367, 531.31, 13, "PanStall1");
			insertStallTiere("PanStall1", "Samuel");
			insertBesitzerTiere("Pan", "PanStall1", 122313999, "Pan");

			c.close();
			
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName()+ ": "+ e.getMessage());
			System.exit(0);
		}
		
	}
	
	public static void loeschen()
	{
		try
		{
			Statement stmt = c.createStatement();
			
			stmt.executeUpdate("drop table Landwirtschaft;");
			stmt.executeUpdate("drop table Stall;");
			stmt.executeUpdate("drop table Besitzer;");
			stmt.executeUpdate("drop table Tiere;");
			stmt.executeUpdate("drop table StallTiere;");
			stmt.executeUpdate("drop table BesitzerTiere;");
			
			stmt.close();
		}
		catch(Exception e)
		{
			System.err.println("Alles löschen abgebrochen!");
		}
		System.out.println("Alles gelöscht!");

	}
	
	public static void createTableLandwirtschaft() throws SQLException
	{
		Statement stmt = c.createStatement();
		String Landwirtschaft="CREATE TABLE IF NOT EXISTS Landwirtschaft("+
				"Vorname TEXT NOT NULL,"+
				"Nachname VARCHAR NOT NULL,"+
				"Steuernummer INTEGER NOT NULL,"+
				"Straßenname TEXT NOT NULL,"+
				"Hausnummer INTEGER NOT NULL,"+
				"Landwirtschaftsname TEXT NOT NULL,"+
				"PRIMARY KEY(Nachname, Steuernummer));";

		stmt.executeUpdate(Landwirtschaft);
		System.out.println("Successfully created Landwirtschaft");
		stmt.close();
	}
	
	public static void insertLandwirtschaft(String Vorname,String Nachname, int Steuernummer, 
			String Straßenname,int Hausnummer, String Landwirtschaftsname) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate("INSERT INTO Landwirtschaft VALUES(\""+Vorname+"\",\""+Nachname+"\",\""+Steuernummer+"\",\""
				+Straßenname+"\",\""+Hausnummer+"\",\""+Landwirtschaftsname+"\");");
		System.out.println("Successfully inserted in Landwirtschaft!");
		stmt.close();
	}
	
	public static void createTableStall() throws SQLException
	{
		Statement stmt = c.createStatement();
		String Stall="CREATE TABLE IF NOT EXISTS Stall("+
				"Bezeichnung VARCHAR PRIMARY KEY NOT NULL,"+
				"Plaetze INTEGER NOT NULL,"+
				"BesetztePlaetze INTEGER,"+
				"Baujahr INTEGER NOT NULL,"+
				"Flaeche REAL NOT NULL);";

		stmt.executeUpdate(Stall);
		System.out.println("Successfully created Stall");
		stmt.close();
	}
	
	public static void insertStall(String Bezeichnung, int Plaetze, 
			int BesetztePlaetze, int Baujahr, double Flaeche) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate("INSERT INTO Stall VALUES(\""+Bezeichnung+"\",\""+Plaetze+"\",\""+BesetztePlaetze+"\",\""
				+Baujahr+"\",\""+Flaeche+"\");");
		System.out.println("Successfully inserted in Stall!");
		stmt.close();
		
	}
	
	public static void createTableBesitzer() throws SQLException
	{
		Statement stmt = c.createStatement();
		String Besitzer="CREATE TABLE IF NOT EXISTS Besitzer("+
				"Vorname TEXT NOT  NULL,"+
				"Nachname VARCHAR NOT NULL,"+
				"Steuernummer INTEGER NOT NULL,"+
				"Straßenname TEXT,"+
				"Hausnummer INTEGER,"+
				"PRIMARY KEY(Nachname, Steuernummer),"+
				"FOREIGN KEY(Nachname, Steuernummer) REFERENCES Landwirtschaft(Nachname, Steuernummer));";

		stmt.executeUpdate(Besitzer);
		System.out.println("Successfully created Besitzer");
		stmt.close();
	}
	
	public static void insertBesitzer(String Vorname, String Nachname, 
			int Steuernummer, String Straßenname, int Hausnummer) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.execute("INSERT INTO Besitzer VALUES(\""+Vorname+"\",\""+Nachname+"\",\""+Steuernummer+"\",\""
				+Straßenname+"\",\""+Hausnummer+"\");");
		System.out.println("Successfully inserted in Besitzer!");
		stmt.close();
		
	}
	
	public static void createTableTiere() throws SQLException
	{
		Statement stmt = c.createStatement();
		String Tiere="CREATE TABLE IF NOT EXISTS Tiere("+
				"Name TEXT NOT NULL,"+
				"Geburtsdatum INTEGER NOT NULL,"+
				"Gewicht REAL NOT NULL,"+
				"Alter1 INTEGER NOT NULL,"+
				"UnterbringungStallBezeichnung VARCHAR NOT NULL,"+
				"PRIMARY KEY(Name, UnterbringungStallBezeichnung));";
	
		stmt.executeUpdate(Tiere);
		System.out.println("Successfully created Tiere");
		stmt.close();
	}
	
	public static void insertTiere(String Name, int Geburtsdatum, 
			double Gewicht, int Alter1, String UnterbringungStallBezeichnung) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate("INSERT INTO Tiere VALUES(\""+Name+"\",\""+Geburtsdatum+"\",\""+Gewicht+"\",\""
				+Alter1+"\",\""+UnterbringungStallBezeichnung+"\");");
		System.out.println("Successfully inserted in Tiere!");
		stmt.close();
		
	}
	
	public static void createTableStallTiere() throws SQLException
	{
		Statement stmt = c.createStatement();
		String StallTiere="CREATE TABLE IF NOT EXISTS StallTiere("+
				"Bezeichnung VARCHAR NOT NULL,"+
				"Name TEXT NOT NULL,"+
				"PRIMARY KEY(Bezeichnung, Name),"+
				"FOREIGN KEY (Bezeichnung) REFERENCES Tiere(UnterbringungStallBezeichnung),"+
				"FOREIGN KEY (Bezeichnung)  REFERENCES Stall(Bezeichnung));";

		stmt.executeUpdate(StallTiere);
		System.out.println("Successfully created StallTiere");
		stmt.close();
	}
	
	public static void insertStallTiere(String Bezeichnung, String Name) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate("INSERT INTO StallTiere VALUES(\""+Bezeichnung+"\",\""+Name+"\");");
		System.out.println("Successfully inserted in StallTiere!");
		stmt.close();
		
	}
	
	public static void createTableBesitzerTiere() throws SQLException
	{
		Statement stmt = c.createStatement();
		String BesitzerTiere="CREATE TABLE IF NOT EXISTS BesitzerTiere("+
				"Nachname VARCHAR NOT NULL,"+
				"UnterbringungStall VARCHAR NOT NULL,"+
				"Kaufdatum INTEGER NOT NULL,"+
				"BesitzerNachname VARCHAR NOT NULL,"+
				"PRIMARY KEY(Nachname, UnterbringungStall, Besitzernachname),"+
				"FOREIGN KEY (BesitzerNachname) REFERENCES Besitzer(Nachname),"+
				"FOREIGN KEY (BesitzerNachname) REFERENCES Stall(BesitzerNachname));";

		stmt.executeUpdate(BesitzerTiere);
		System.out.println("Successfully created BesitzerTiere");
		stmt.close();
	}
	
	public static void insertBesitzerTiere(String Nachname, String UnterbringungStall, int Kaufdatum, 
			String BesitzerNachname) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate("INSERT INTO BesitzerTiere VALUES(\""+Nachname+"\",\""+UnterbringungStall
				+"\",\""+Kaufdatum+"\",\""+BesitzerNachname+"\");");
		System.out.println("Successfully inserted in BesitzerTiere!");
		stmt.close();
		
	}

}
