import java.sql.*;


public class Main {

	public static void main(String[] args) {
		
		boolean clearen=false;
		Connection c = null;
		Statement stmt = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection("jdbc:sqlite:Spieleanbieter.db");
			
			System.out.println("Opened Database successfully");
			
			stmt = c.createStatement();
			
			if(clearen==true)
			{
				stmt.executeUpdate("drop table Publisher");
				stmt.executeUpdate("drop table Support");
				stmt.executeUpdate("drop table Supporter");
				stmt.executeUpdate("drop table Programmierer");
				stmt.executeUpdate("drop table Grafiker");
				stmt.executeUpdate("drop table Lohn");
				stmt.executeUpdate("drop table Kunde");
				stmt.executeUpdate("drop table OnlineSpieleanbieter");
				stmt.executeUpdate("drop table VerschiedeneSpiele");
			}
			
			stmt.executeUpdate("PRAGMA foreign_keys = ON;");
			
			String sql="create table IF NOT EXISTS OnlineSpieleanbieter"+
					"(Artikelnummer INTEGER PRIMARY KEY NOT NULL,"+
					"Anbieter TEXT,"+
					"Preis REAL,"+
					"FOREIGN KEY (Artikelnummer) REFERENCES Publisher (ArtikelnummerP));";
			
			
			String sql2="create table IF NOT EXISTS Kunde"+
					"(SpielerID INTEGER PRIMARY KEY NOT NULL,"+
					"Budget REAL,"+
					"SpieleImBesitz INTEGER)";
			
			
			String sql3="create table IF NOT EXISTS VerschiedeneSpiele"+
					"(SpielerID INTEGER PRIMARY KEY NOT NULL,"+
					"Anbieter TEXT,"+
					"Preis REAL,"+
					"FOREIGN KEY (SpielerID) REFERENCES Kunde (SpielerID));";
			
			
			String sql4="create table IF NOT EXISTS Publisher"+
					"(ArtikelnummerP INTEGER PRIMARY KEY NOT NULL,"+
					"Mitarbeiter INTEGER)";
			
			
			String sql5="create table IF NOT EXISTS Support"+
					"(Mitarbeiter INTEGER PRIMARY KEY NOT NULL)";
			
			
			String sql6="create table IF NOT EXISTS Supporter"+
					"(MitarbeiterID INTEGER PRIMARY KEY NOT NULL)";			
			
			
			String sql7="create table IF NOT EXISTS Programmierer"+
					"(MitarbeiterID INTEGER PRIMARY KEY NOT NULL)";			
			
			
			String sql8="create table IF NOT EXISTS Grafiker"+
					"(MitarbeiterID INTEGER PRIMARY KEY NOT NULL)";			
			
			
			String sql9="create table IF NOT EXISTS Lohn"+
					"(GrafikerMitarbeiterID INTEGER,"+	
					"ProgrammiererMitarbeiterID INTEGER,"+	
					"SupporterMitarbeiterID INTEGER,"+	
					"Lohn REAL,"+
					"Arbeitsstunden REAL,"+
					"MitarbeiterName TEXT,"+
					"PRIMARY KEY (GrafikerMitarbeiterID, ProgrammiererMitarbeiterID, SupporterMitarbeiterID),"+
					"FOREIGN KEY (GrafikerMitarbeiterID) REFERENCES Grafiker(MitarbeiterID),"+
					"FOREIGN KEY (ProgrammiererMitarbeiterID) REFERENCES Programmierer(MitarbeiterID),"+
					"FOREIGN KEY (SupporterMitarbeiterID) REFERENCES Supporter (MitarbeiterID));";
					
			stmt.executeUpdate(sql4);
			System.out.println("erstellt");
			stmt.executeUpdate(sql5);
			System.out.println("erstellt");
			stmt.executeUpdate(sql6);		
			System.out.println("erstellt");
			stmt.executeUpdate(sql7);
			System.out.println("erstellt");
			stmt.executeUpdate(sql8);
			System.out.println("erstellt");
			stmt.executeUpdate(sql9);
			System.out.println("erstellt");
			stmt.executeUpdate(sql2);
			System.out.println("erstellt");
			stmt.executeUpdate(sql);
			System.out.println("erstellt");
			stmt.executeUpdate(sql3);
			System.out.println("erstellt");
		

			stmt.close();
		    c.close();
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName()+ ": "+ e.getMessage());
			System.exit(0);
		}
		System.out.println("Database successfully created!");
		
	}

}