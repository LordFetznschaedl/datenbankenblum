import java.sql.*;
import java.util.ArrayList;
public class Main {

	public static String[] tablenames = {"kunden","sitzplaetze","waggonklasse"};
	public static String[] namen = {"Hans","Peter","Gabriel","Franz","Sep"};
	public static Connection c = null;
	
	
	
	public static void main(String[] args) {
		
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection("jdbc:sqlite:WaggonBeispiel-KW10.db");
			System.out.println("Opened Databank successfully");
			
			dropDatabases();
			
			System.out.println("alles gelöscht");
			
			
			createTables("CREATE TABLE IF NOT EXISTS kunden("
					+ "kundenID INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, sitzplatz INTEGER);");
			createTables("CREATE TABLE IF NOT EXISTS sitzplaetze("
					+ "sitzplatzID INTEGER PRIMARY KEY, waggonID INTEGER, FOREIGN KEY(sitzplatzID) REFERENCES kunden(sitzplatz));");
			createTables("CREATE TABLE IF NOT EXISTS waggonklasse("
					+ "waggonID INTEGER PRIMARY KEY, klasse INTEGER, CHECK (klasse IN (1,2)), FOREIGN KEY (waggonID) REFERENCES sitzplaetze(waggonID));");
			
			System.out.println("alles created");
			

			for(int i=0;i<namen.length;i++)
			{
				insertKunden(namen[i],i+1);
			}
			System.out.println("alle kunden inserted");

			int waggonNr=1;
			for(int i=1;i<=10;i++)
			{
				if(i%5==0)
				{
					insertWaggonklasse(waggonNr,1);
					waggonNr++;
				}
				insertSitzplaetze(i,waggonNr);
			}
			System.out.println("alles sitzplätze & waggone insertiert");
			
			selectStmt("select name, sitzplatz from  kunden where sitzplatz  IN (select sitzplatzID from sitzplaetze where waggonID IN (select waggonID from waggonklasse where klasse = 1 ));");
			
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName()+ ": "+ e.getMessage());
			System.exit(0);
		}
	}
	
	public static void dropDatabases() throws SQLException
	{
		Statement stmt = c.createStatement();
		for(int i=0;i<tablenames.length;i++)
		{
			try{
				stmt.executeUpdate("drop table "+tablenames[i]+";");
			}
			catch(Exception e)
			{
				System.err.println("Tabelle konnte nicht gedropt werden");
			}
		}
		stmt.close();
	}
	
	public static void createTables(String createStatement) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate(createStatement);
		stmt.close();
	}
	
	public static void insertKunden(String sName, int sitzplatz) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate("insert into kunden (name, sitzplatz) values (\""+sName+"\","+sitzplatz+");");
		stmt.close();
	}
	
	public static void insertSitzplaetze(int sitzplatzID, int waggonID) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate("insert into sitzplaetze values("+sitzplatzID+","+waggonID+");");
		stmt.close();
	}
	
	public static void insertWaggonklasse(int waggonID, int klasse) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate("insert into waggonklasse values ("+waggonID+","+klasse+");");
		stmt.close();
	}
	
	public static void selectStmt(String sStmt) throws SQLException
	{
		Statement stmt = c.createStatement();
		System.out.println(stmt.executeUpdate(sStmt));
		stmt.close();
	}

}
