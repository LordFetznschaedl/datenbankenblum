#Datenbanken-Repository#
   
Links können nicht angeklickt werden, da es sonst nur auf einen Dead-Link geht.   
Die Links kann man kopieren und in einem neuen Tap einfügen, dann funktionieren die Links.   
   
*Bartenbach Florian:*   
[https://Mapple_the_Apple@bitbucket.org/Mapple_the_Apple/dbs_abgaben.git](Link URL)

*Biechl Matthias:*   
[https://Heagl@bitbucket.org/Heagl/infi-dbs-16-17.git](Link URL)   
 
*Krapf Andreas:*   
[https://bitbucket.org/MisteX/infi-dbs-hus](Link URL)

*Flatscher Samuel:*   
[https://bitbucket.org/Ramagos/](Link URL)
    
*Eller Thomas:*   
[https://bitbucket.org/tomker98/infi-dbs](Link URL)       
[https://bitbucket.org/tomker98/infi-dm](Link URL)
    
*Biechl Daniel:*    
[https://Biechl_D@bitbucket.org/Biechl_D/infi-dbs_3ahwii_biechl_daniel.git](Link URL)   
   
*Pal Maximilian:*    
[https://PalMax98@bitbucket.org/PalMax98/dm.git](Link URL)     
     
*Thattarettu Jebin:*    
[https://bitbucket.org/jebinTJ/](Link URL)     
   
*Pejic Martin:*    
[https://MartinPejic@bitbucket.org/MartinPejic/htl_martin.git](Link URL)   
    
*Schonner Emma:*     
[https://github.com/EmmaSchonner](Link URL)      
    
*Moser Christoph:*     
[https://github.com/ChristophMoserGreil/INFI_HUE_5](Link URL)
    
![HTLinn_Logo_d51ed239c6.gif](https://bitbucket.org/repo/gkbGgMx/images/1334276955-HTLinn_Logo_d51ed239c6.gif)